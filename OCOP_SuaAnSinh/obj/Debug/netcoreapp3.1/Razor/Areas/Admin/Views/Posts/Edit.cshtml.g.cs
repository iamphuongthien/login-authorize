#pragma checksum "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d02f3ca023596d9f94f49904d5fa5c232038d34b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Posts_Edit), @"mvc.1.0.view", @"/Areas/Admin/Views/Posts/Edit.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d02f3ca023596d9f94f49904d5fa5c232038d34b", @"/Areas/Admin/Views/Posts/Edit.cshtml")]
    public class Areas_Admin_Views_Posts_Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<OCOP_SuaAnSinh.Models.Post>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n");
#nullable restore
#line 3 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Edit.cshtml"
  
    ViewData["Title"] = "Edit";
    Layout = "~/Areas/Admin/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<h1>Edit</h1>

<h4>Post</h4>
<hr />
<div class=""row"">
    <div class=""col-md-4"">
        <form asp-action=""Edit"">
            <div asp-validation-summary=""ModelOnly"" class=""text-danger""></div>
            <input type=""hidden"" asp-for=""PostId"" />
            <div class=""form-group"">
                <label asp-for=""Title"" class=""control-label""></label>
                <input asp-for=""Title"" class=""form-control"" />
                <span asp-validation-for=""Title"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""Abstract"" class=""control-label""></label>
                <input asp-for=""Abstract"" class=""form-control"" />
                <span asp-validation-for=""Abstract"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""Content"" class=""control-label""></label>
                <input asp-for=""Content"" class=""form-control"" />
                <span asp-validation-for=""Content"" class=""text");
            WriteLiteral(@"-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""ContentHTML"" class=""control-label""></label>
                <input asp-for=""ContentHTML"" class=""form-control"" />
                <span asp-validation-for=""ContentHTML"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""Tags"" class=""control-label""></label>
                <input asp-for=""Tags"" class=""form-control"" />
                <span asp-validation-for=""Tags"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""Author"" class=""control-label""></label>
                <input asp-for=""Author"" class=""form-control"" />
                <span asp-validation-for=""Author"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""PostCategoryId"" class=""control-label""></label>
                <select asp-for=""PostCategoryId"" class=""");
            WriteLiteral(@"form-control"" asp-items=""ViewBag.PostCategoryId""></select>
                <span asp-validation-for=""PostCategoryId"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""UserId"" class=""control-label""></label>
                <input asp-for=""UserId"" class=""form-control"" />
                <span asp-validation-for=""UserId"" class=""text-danger""></span>
            </div>
            <div class=""form-group form-check"">
                <label class=""form-check-label"">
                    <input class=""form-check-input"" asp-for=""IsPublish"" /> ");
#nullable restore
#line 59 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Edit.cshtml"
                                                                      Write(Html.DisplayNameFor(model => model.IsPublish));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                </label>\n            </div>\n            <div class=\"form-group form-check\">\n                <label class=\"form-check-label\">\n                    <input class=\"form-check-input\" asp-for=\"IsHot\" /> ");
#nullable restore
#line 64 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Edit.cshtml"
                                                                  Write(Html.DisplayNameFor(model => model.IsHot));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                </label>
            </div>
            <div class=""form-group"">
                <label asp-for=""ImageUrl"" class=""control-label""></label>
                <input asp-for=""ImageUrl"" class=""form-control"" />
                <span asp-validation-for=""ImageUrl"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""PathUrl"" class=""control-label""></label>
                <input asp-for=""PathUrl"" class=""form-control"" />
                <span asp-validation-for=""PathUrl"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""Date"" class=""control-label""></label>
                <input asp-for=""Date"" class=""form-control"" />
                <span asp-validation-for=""Date"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""CreatedDate"" class=""control-label""></label>
                <input asp-for=""CreatedDate"" class=""form");
            WriteLiteral(@"-control"" />
                <span asp-validation-for=""CreatedDate"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""ModifyDate"" class=""control-label""></label>
                <input asp-for=""ModifyDate"" class=""form-control"" />
                <span asp-validation-for=""ModifyDate"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""ModifyBy"" class=""control-label""></label>
                <input asp-for=""ModifyBy"" class=""form-control"" />
                <span asp-validation-for=""ModifyBy"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""Language"" class=""control-label""></label>
                <input asp-for=""Language"" class=""form-control"" />
                <span asp-validation-for=""Language"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""Status"" c");
            WriteLiteral(@"lass=""control-label""></label>
                <input asp-for=""Status"" class=""form-control"" />
                <span asp-validation-for=""Status"" class=""text-danger""></span>
            </div>
            <div class=""form-group form-check"">
                <label class=""form-check-label"">
                    <input class=""form-check-input"" asp-for=""IsActive"" /> ");
#nullable restore
#line 109 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Edit.cshtml"
                                                                     Write(Html.DisplayNameFor(model => model.IsActive));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                </label>\n            </div>\n            <div class=\"form-group form-check\">\n                <label class=\"form-check-label\">\n                    <input class=\"form-check-input\" asp-for=\"IsConstance\" /> ");
#nullable restore
#line 114 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Edit.cshtml"
                                                                        Write(Html.DisplayNameFor(model => model.IsConstance));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                </label>
            </div>
            <div class=""form-group"">
                <label asp-for=""NumberOfView"" class=""control-label""></label>
                <input asp-for=""NumberOfView"" class=""form-control"" />
                <span asp-validation-for=""NumberOfView"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <input type=""submit"" value=""Save"" class=""btn btn-primary"" />
            </div>
        </form>
    </div>
</div>

<div>
    <a asp-action=""Index"">Back to List</a>
</div>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<OCOP_SuaAnSinh.Models.Post> Html { get; private set; }
    }
}
#pragma warning restore 1591

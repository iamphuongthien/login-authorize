#pragma checksum "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5ac36926f6ddb73a926e8c2599802da7df94200a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Posts_Delete), @"mvc.1.0.view", @"/Areas/Admin/Views/Posts/Delete.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5ac36926f6ddb73a926e8c2599802da7df94200a", @"/Areas/Admin/Views/Posts/Delete.cshtml")]
    public class Areas_Admin_Views_Posts_Delete : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<OCOP_SuaAnSinh.Models.Post>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n");
#nullable restore
#line 3 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
  
    ViewData["Title"] = "Delete";
    Layout = "~/Areas/Admin/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\n<h1>Delete</h1>\n\n<h3>Are you sure you want to delete this?</h3>\n<div>\n    <h4>Post</h4>\n    <hr />\n    <dl class=\"row\">\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 16 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Title));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 19 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Title));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 22 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Abstract));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 25 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Abstract));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 28 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Content));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 31 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Content));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 34 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.ContentHTML));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 37 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.ContentHTML));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 40 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Tags));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 43 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Tags));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 46 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Author));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 49 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Author));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 52 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.UserId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 55 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.UserId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 58 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.IsPublish));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 61 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.IsPublish));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 64 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.IsHot));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 67 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.IsHot));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 70 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.ImageUrl));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 73 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.ImageUrl));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 76 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.PathUrl));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 79 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.PathUrl));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 82 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.PostCategory));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 85 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.PostCategory.PostCategoryId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd class>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 88 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Date));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 91 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Date));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 94 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.CreatedDate));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 97 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.CreatedDate));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 100 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.ModifyDate));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 103 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.ModifyDate));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 106 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.ModifyBy));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 109 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.ModifyBy));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 112 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Language));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 115 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Language));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 118 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Status));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 121 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Status));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 124 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.IsActive));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 127 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.IsActive));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 130 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.IsConstance));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 133 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.IsConstance));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n        <dt class = \"col-sm-2\">\n            ");
#nullable restore
#line 136 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.NumberOfView));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dt>\n        <dd class = \"col-sm-10\">\n            ");
#nullable restore
#line 139 "E:\WebQN\ocop_suaansinh_core-master\OCOP_SuaAnSinh\Areas\Admin\Views\Posts\Delete.cshtml"
       Write(Html.DisplayFor(model => model.NumberOfView));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </dd>\n    </dl>\n    \n    <form asp-action=\"Delete\">\n        <input type=\"hidden\" asp-for=\"PostId\" />\n        <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\" /> |\n        <a asp-action=\"Index\">Back to List</a>\n    </form>\n</div>\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<OCOP_SuaAnSinh.Models.Post> Html { get; private set; }
    }
}
#pragma warning restore 1591
